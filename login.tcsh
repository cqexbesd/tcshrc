# Commands to be run once per session rather than for every shell. Much of
# this is inherited by child shells but by not setting it each time we allow
# it to be changed when needed.
#
# $Id: login.tcsh,v 1.19 2009/04/17 18:44:32 andrew Exp $

umask 022

# If the kernel doesn't know our temrinal size then query for it from our
# terminal (emulator).
if (-x /usr/bin/resizewin) then
	/usr/bin/resizewin -z
endif

if (-X mesg) then
	if ($?prefs_mesgyorn) then
		mesg $prefs_mesgyorn
	else
		mesg y
	endif
endif

# make sure the user has .hushlogin as we display the motd...don't need the
# system to do it for us. i always forget to create this so i thought I'd
# cheat
if (! -f ~/.hushlogin) then
	touch ~/.hushlogin
endif

# try to set up our terminal
# this is based on a few rules:
#	1) MS Telnet, the dog that it is, identifies itself as "ansi". it
#	   appears to be vt102 based on informal testing with curses apps.
#      The later terminfo files have an entry ms-vt100-color just for it
#	2) Solaris CDE Terminal Emulator sets it's terminal type to
#      "CDE Terminal Emulator" however you can't use a string with spaces as
#      an argument to tset -m so we map this to dtterm (the canonical name)
#	3) Solaris doesn't know of FreeBSD's cons25 but instead has scoconsole
#	4) Solaris doesn't/didn't know about xterm-color
#	5) Terminal.app used to claim its terminal type as d0 under 10.1
#	6) Terminal.app requires TermCapString to be set to nsterm-c in
#	   ~/Library/Preferences/com.apple.Terminal.plist manually. If it's not
#	   you can guess based on TERM_PROGRAM=Apple_Terminal
#
# there are in fact 2 methods of testing for a working terminal here - the
# prefered method is to use termname but if it isn't available then to use
# tset < /dev/null. We use tset - rather than -q as Solaris does not yet
# support -q. Using tset can cause errors to go to the terminal on login but
# they can be ignored. termname appeared in tcsh 6.13.01.

# first we just indicate if we have found a term type to try
set temp_termset = 0

# select a method os testing for terminal entries
if ( -X termname ) then
	set temp_termname = termname
	set temp_input_redirection = ''
else
	set temp_termname = 'tset - -Q'
	set temp_input_redirection = '< /dev/null'
endif

# then catch the terminal name with spaces problem
if (XXX`eval "$temp_termname $temp_input_redirection"` == 'XXXCDE terminal emulator') then
	set temp_term_type = dtterm
	set temp_termset = 1
endif

# now check for Terminal.app without the manual prefs hack
if ($?prefs_term_forceTerminalApp) then
	if ($?TERM_PROGRAM) then
		if ("XXX$TERM_PROGRAM" == 'XXXApple_Terminal') then
			set temp_term_type = nsterm-c
			set temp_termset = 1
		endif
	endif
endif

# now, if we haven't worked it out yet, we check to see if there is a mapping
# in typemappings
#
# due to tset stupidness this might causes errors to appear at login if the
# users terminal has an entry with suggestions that don't exist in this
# systems terminfo DB
if (! $temp_termset && $?prefs_term_typemappings) then
	# check for a match in our prefs array
	foreach temp_term_map ($prefs_term_typemappings:q)
		# split each line up into an array
		set temp_term_map = ($temp_term_map:x)
		# check to see if this mapping line is for our current term type. we
		# try lowercasing the term type as well as telnet terminal type
		# option negotiation isn't case sensitive and telneting from a solaris
		# box to an AIX ones ends up with TERM set to your terminal in upper
		# case
		if ("${TERM:al}" == "${temp_term_map[1]:al}") then
			# our term type matches this line, try each term type in turn till
			# we have a working one.
			foreach temp_term_type ($temp_term_map[2-]:q)
				# this sucks...we don't want stderr going to the terminal
				# however tset requires it - for no good reason, we tell it not
				# to do any initialisation and provide a term type. bah! I
				# should just right my own config tool except it would be one
				# more thing to distribute
				eval "$temp_termname ${temp_term_type} $temp_input_redirection" > /dev/null
				if (! $status) then
					# we have a working type in $temp_term_type
					set temp_termset = 1
					break; break;
				endif
			end
		endif
	end
endif

# perhaps tset can just guess - usually the nicest option
if (! $temp_termset) then
	set temp_term_type = "`eval \"$temp_termname $temp_input_redirection\"`"
	if (! $status) then
		set temp_termset = 1
	endif
endif

# nothing has worked so far so just use whatever was set as the default
if (! $temp_termset && $?prefs_term_default) then
	# use the default
	set temp_term_type = "$prefs_term_default"
	set temp_termset = 1
endif

# now if we have selected a type then actually initialise the terminal
if ($temp_termset) then
	# the terminal type may start with a ? so we have to run it through
	# tset -m. It's no wonder people have scripts for this crap.
	# sadly tset under Solaris doesn't use terminfo so it's not safe to
	# call it. we make calling tset conditional on this not being
	# solaris but then you can't use the ? trick to be prompted for a
	# terminal type and something else needs to initialise the terminal
	# ...this needs more research. 
	switch($OENV)
	case 'freebsd':
		eval "`tset -s -Q -m ${TERM}:$temp_term_type:q`"
		breaksw
	default:
		setenv TERM $temp_term_type
		breaksw
	endsw
endif

# clean up
unset temp_termset temp_term_map temp_term_type temp_termname temp_input_redirection

# set DISPLAY is not set but we might be running X. not perfect but the best
# we can probably manage without shipping a binary X client of some sort
if ($?prefs_guess_display) then
	if (! ${?DISPLAY}) then
		if (${?REMOTEHOST}) then
			foreach temp_term_type (${prefs_guess_display:q})
				if ("${TERM:al}" == ${temp_term_type:al}) then
					# terminal type indicates we may be running X
					setenv DISPLAY ${REMOTEHOST}:0.0
					break;
				endif
			end
			unset temp_term_type
		endif
	endif
endif

# start ssh-agent if so configured and it's not running
if ($?prefs_start_ssh_agent) then
	if (! ${?SSH_AUTH_SOCK}) then
		# we want to share an agent between shells if we start it this
		# way
		setenv SSH_AUTH_SOCK ${HOME}/.ssh.agent.sock
		ssh-add -l >& /dev/null
		if ( $? ) then
			# not running yet
			eval `ssh-agent -c -a ${SSH_AUTH_SOCK}`
		endif
	endif
endif

# display stuff to user

# show the motd if changed. use hostname in the filename to cope with NFS
# mounted ~ (this can cause a few files to be created on machines that get
# their hostname from DHCP)
if (-f /etc/motd) then
	# a bit of a hack in case this is the first time we have run. we can't just
	# use the lazy execution of || because tcsh always runs things in backquotes
	if (-f ~/.lastmotdmod.$HOST) then
		set lastmod = `cat ~/.lastmotdmod.$HOST`
	else
		set lastmod = 'non-number'
	endif
	if (-M /etc/motd != $lastmod) then
		filetest -M /etc/motd > ~/.lastmotdmod.$HOST
		$PAGER /etc/motd
		# a blank line to sperate motd from what comes next
		echo
	endif
	unset lastmod
endif

# check quota
if ($?prefs_check_quota) then
	if (-X quota) then
		switch ($OENV)
		case 'hpux':
		case 'solaris':
			quota
			breaksw
		case 'linux':
			quota -qs
			breaksw
		case 'freebsd':
			quota -hq
			breaksw
		default:
			quota -q
			breaksw
		endsw
	endif
endif

# check for system messages
if (-X msgs) then
	msgs -q
endif

# check for new mail. if elm is installed then newmail is probably not the
# one we are thinking off. hmm...need a better workaround than this
if (-X newmail && ! -X elm) then
	if ($?prefs_incoming_mail) then
		newmail -r $MAIL $prefs_incoming_mail
	else
		newmail -r $MAIL
	endif
endif

# display a fortune cookie
if (-X fortune) then
	fortune all
endif
