# enable various tcsh features
#
# $Id: features.tcsh,v 1.15 2013/10/06 17:32:47 andrew Exp $

# 24 hour clock by default
unset ampm
# Correct spelling when trying completion
set autocorrect
# List other possibilities after ambiguous completion
set autolist
# set up auto locking/logout (lock doesn't work BTW)
if ($?REMOTEHOST) then
	# remote login
	if (${?prefs_autologout_domain}) then
		foreach temp_domain_map (${prefs_autologout_domain:q})
			set temp_domain_map = ($temp_domain_map:x)
			if (${REMOTEHOST:al} ${temp_positive_glob} ${temp_domain_map[1]:al:q}) then
				set autologout = (${temp_domain_map[2]} ${temp_domain_map[3]})
				set temp_al_set
				break;
			endif
		end
		unset temp_domain_map
	endif
	if (! ${?temp_al_set}) then
		# check to see if it is an unqualified hostname
		if (${?prefs_autologout_unqualified}) then
			if (${REMOTEHOST} ${temp_negative_glob} '.') then
				set autologout = (${prefs_autologout_unqualified})
				set temp_al_set
			endif
		endif
	endif
	if (! ${?temp_al_set}) then
		if (${?prefs_autologout}) then
			set autologout = (${prefs_autologout})
		endif
	else
		unset temp_al_set
	endif
else
	# local window
	if (${?prefs_autologout_local}) then
		set autologout = (${prefs_autologout_local})
	endif
endif
# rehash on command not found
set autorehash
# Allow escaping quotes (with '\') in strings
set backslash_quote
# When cding, if the directory is not found, try looking here.
set cdpath = ( ~ / )
if (-d ~/.wormholes) then
	set cdpath = ( ~/.wormholes $cdpath)
endif
if (${?prefs_cdpath_add_first}) then
	set cdpath = (${prefs_cdpath_add_first} $cdpath)
endif
if (${?prefs_cdpath_add_last}) then
	set cdpath = ($cdpath ${prefs_cdpath_add_last})
endif
# Use color in ls (see also the ENV variable LS_COLORS)
switch($OENV)
case 'darwin':
case 'solaris':
	# /bin/ls doesn't support colour
	set color = ls-F
	breaksw
default:
	set color
	breaksw
endsw
# Set so completion is case insensitive but I wish ., _ and - were
# still different. Anyone know the suggestions address for tcsh?
set complete = Enhance
# Just in case
set WonK
# Use both SysV and BSD style echo args
set echo_style = both
# If prompt path is to long...
set ellipsis
# Only keep one copy of repeated commands.
set histdup = erase
# Remember last 1024 commands
set history = 1024
set savehist = (6144 merge)
# enable ^D to logout - this is default but certain Linux distros ship badly
# misconfigured
unset ignoreof
# Show pids on suspension
set listjobs = long
# Enable new mail checking...this never used to work...
set mail = ($MAIL)
# For those of us who only know 3 characters...
set nokanji
# Have a prompt thats the envy of your friends but still fits on the screen :-)
# this is set by default under Fedora Linux
unset promptchars
set prompt = "%n@%m"
# SCHROOT is a jail like thing on linux
if (${?SCHROOT_CHROOT_NAME}) then
	set prompt = "${prompt}(${SCHROOT_CHROOT_NAME})"
endif
# Clearcase is an SCM from IBM
if (${?CLEARCASE_ROOT} && -X cleartool) then
	# if we are in a clearcase shell we want to know the working view...
	# if our view name is in the lookup table then we place it's shortname
	# in the prompt, else we place its full name
	set temp_view_name = `cleartool pwv -short -set`
	if (${?prefs_clearcase_viewnames}) then
		# we have a lookup table so iterate through each line
		foreach temp_view_map (${prefs_clearcase_viewnames:q})
			# split the line up into words
			set temp_view_map = (${temp_view_map:x})
			if ("${temp_view_name}" == "${temp_view_map[1]}") then
				# we have found it
				set temp_view_name = ${temp_view_map[2]}
				break;
			endif
		end
	endif
	set prompt = "${prompt}[${temp_view_name}]"
	unset temp_view_map temp_view_name
endif
set prompt = "${prompt}%B%c02%b%#%L"
# http://www.ugh.net.au/~andrew/rm.html
set rmstar
# Pretend symlink directories are real directories for .. purposes
set symlinks = ignore
# Give an indication of how long a command ran
set time = ( 1 'Actual: %E User: %U Kernel: %S CPU: %P Swapped: %W' )
