# configure paths, aliases and misc environment variables
#
# $Id: commands_and_environment.tcsh,v 1.99 2013/10/06 17:30:34 andrew Exp $

### Path

# work around for a few broken hosts I used to have accounts on
if ($?prefs_path_screwed) then
	# path is so bad we should overwrite it with something better
	set path = ({/usr{/local,},}/bin)
endif
# now add other stuff
switch($OENV)
case 'darwin':
	set -f path = (~/bin /opt/local/{bin,sbin} ${path:q} {/usr{,/local},}/sbin /Developer/Tools /Developer/usr/{bin,sbin})
	breaksw
case 'FreeBSD':
	set -f path = (~/bin ${path:q} {/usr{/local,},}/sbin /usr/games /usr/local/pgsql/bin)
	breaksw
case 'solaris':
	set -f path = (~/bin /usr/pkg/bin ${path:q} /usr/pkg/sbin {/usr{/local,},}/sbin /usr/xpg4/bin /opt/SUNWspro/bin {/usr/ccs,/usr/dt,/usr/openwin}/bin /usr/ucb)
	breaksw    
case 'aix':
	set -f path = (~/bin ${path:q} {/usr{/local,},}/sbin /usr/vac/bin)
	breaksw
case 'hpux':
	set -f path = (~/bin ${path:q} {/usr{/local,},}/sbin /usr/ccs/bin)
	breaksw
case 'cygwin':
	set -f path = (~/bin ${path:q} {/usr{/local,},}/sbin)
	breaksw
default:
	set -f path = (~/bin ~/.local/bin ${path:q} /usr/pkg/{,s}bin {/usr{/local,},}/sbin)
	breaksw
endsw
# add user and platform specific directories
if ( ${?OENV} ) then
	if ( -d ~/bin/${OENV} ) then
		set -f path = (~/bin/${OENV} $path)
	endif
endif
if ( ${?HOST_DOUBLET} ) then
	if ( -d ~/bin/${HOST_DOUBLET} ) then
		set -f path = (~/bin/${HOST_DOUBLET} $path)
	endif
endif
if ( ${?HOST_TRIPLET} ) then
	if ( -d ~/bin/${HOST_TRIPLET} ) then
		set -f path = (~/bin/${HOST_TRIPLET} $path)
	endif
endif
	endif
	if ( ${?ARCH} ) then
		if ( -d ~/bin/${OENV}-${ARCH} ) then
			set -f path = (~/bin/${OENV}-${ARCH} $path)
		endif
	endif
endif
# add the users specified directories
if ($?prefs_path_add_first) then
	set -f path = ($prefs_path_add_first $path)
endif
if ($?prefs_path_add_last) then
	set -f path = ($path $prefs_path_add_last)
endif

### Environment and other settings

setenv BLOCKSIZE K
if (-X clang) then
	setenv CC clang
	setenv LD clang
else if (-X gcc) then
	setenv CC gcc
	setenv LD gcc
endif
if (-X clang++) then
	setenv CXX clang++
else if (-X g++) then
	setenv CXX g++
endif
setenv MAIL /var/mail/$USER

# ARCHIE
if ($?prefs_archie_server) then
	setenv ARCHIE_HOST $prefs_archie_server
else
	setenv ARCHIE_HOST archie.au
endif

# CD Player
if ($?prefs_cd_audio_dev) then
	setenv CDROM $prefs_cd_audio_dev
else
	if (-f /dev/cdrom) then
		setenv CDROM /dev/cdrom
	else
		setenv CDROM /dev/acd0c
	endif
endif

# core dump names
switch($OENV)
case 'solaris':
	if (-X coreadm) then
		if ((${OENVVER:r} > 5) || ((${OENVVER:r} == 5) && (${OENVVER:e} > 8))) then
			coreadm -p '%f.core'
		else
			coreadm -p '%f.core' $$
		endif
	endif
	breaksw
endsw

# CVS
setenv CVS_RSH ssh
if ($?prefs_cvs_root) then
	setenv CVSROOT $prefs_cvs_root
endif

# EDITOR
foreach tmp_editor (nvim vim vim.basic vim.tiny nvi vi)
	if (-X ${tmp_editor}) then
		if (${tmp_editor} != vi) then
			alias vi ${tmp_editor}
			# shouldn't there be a viewim?
			alias view ${tmp_editor} -R
		endif
		setenv EDITOR `which ${tmp_editor}`
		break
	endif
end
unset tmp_editor

# IRC
if ($?prefs_irc_nick) then
	setenv IRCNICK "$prefs_irc_nick"
endif
if ($?prefs_irc_name) then
	setenv IRCNAME "$prefs_irc_name"
endif
if ($?prefs_irc_server) then
	setenv IRCSERVER "$prefs_irc_server"
endif
if (-X epic) then
	alias irc epic
endif

# attempt to get standard english with metric and euros
setenv LANG en_GB.UTF-8
setenv LC_CTYPE en_GB.UTF-8
setenv LC_MONETARY de_DE.UTF-8
setenv LC_MEASUREMENT de_DE.UTF-8

# look for my custom makefiles if available
if (-d ~/src/Makefiles) then
	setenv MAKEFLAGS        "-I$HOME/src/Makefiles"
endif

# NNTPSERVER
if ($?prefs_nntp_server) then
	setenv NNTPSERVER $prefs_nntp_server
endif

# PAGER
setenv MORE '-iF' # options for more if we end up running it
setenv LESS '-iMx4XrF' # options for less if we end up running it
# PAGER needs to be set for other things (such as psm) to work try hard
# to set it to something even if it's not great
if (-X less) then
	setenv PAGER less
	alias more 'less'
else if ($OENV == 'solaris' && -x '/usr/xpg4/bin/more') then
		setenv PAGER '/usr/xpg4/bin/more'
		alias more '/usr/xpg4/bin/more'
else if (-X more) then
	setenv PAGER more
else
	setenv PAGER cat
endif
# don't resolve till run time in case the user changes their mind
alias m \$PAGER

# use a pre filled out PR form if it exists
if (-f ~/info/pr.template) then
	setenv PR_FORM ~/info/pr.template
endif

# PRINTER
setenv PRINTER lp
if ($?prefs_printer) then
	setenv PRINTER $prefs_printer
endif

# PROXIES
if ($?prefs_http_proxy) then
	setenv HTTP_PROXY $prefs_http_proxy
endif
if ($?prefs_http_proxy_auth) then
	setenv HTTP_PROXY_AUTH $prefs_http_proxy_auth
endif

# SGML
setenv SGML_ROOT /usr/local/share/sgml
setenv SGML_CATALOG_FILES ${SGML_ROOT}/jade/catalog
setenv SGML_CATALOG_FILES ${SGML_ROOT}/iso8879/catalog:$SGML_CATALOG_FILES
setenv SGML_CATALOG_FILES ${SGML_ROOT}/html/catalog:$SGML_CATALOG_FILES
setenv SGML_CATALOG_FILES ${SGML_ROOT}/docbook/catalog:$SGML_CATALOG_FILES

# SHELL
# we wan't to be sure SHELL is set to something sensible. this test is split
# in two because if SHELL isn't set at all we can't even refer to it in the
# test - short circuiting doesn't help because the entire line is variable
# expanded first
if (${?SHELL}) then
	set temp_shell = "${SHELL}"
else
	set temp_shell = ''
endif
if ("${temp_shell}" ${temp_negative_glob} '*tcsh') then
	# either SHELL isn't set (cygwin does this) or it is set to something
	# other than tcsh (perhaps tcsh was execed by another shell and we are
	# on a system without enough priveleges to change our shell). this can
	# confuse things such as tset so try to correct this
	if (-x `which tcsh`) then
		setenv SHELL `which tcsh`
	else if ( -X tcsh) then
		setenv SHELL tcsh
	endif
endif
unset temp_shell

# TZ
if ($?prefs_time_zone) then
	if (XXX${prefs_time_zone} != XXX) then
		setenv TZ $prefs_time_zone
	endif
endif

# UNIX95
# Under HPUX setting UNIX95 makes commands perform more like the UNIX 95
# standard - which is good for us as I don't know any HPUX peculiarities.
switch($OENV)
case 'hpux':
	setenv UNIX95
	breaksw
endsw


### Aliases

# selecting prefered versions of software (needs to happen before other
# aliasing)

# those that can be done by version number
if (-X jot) then
	# perl - not really needed as no one ships anything but perl5 now days
	if (-X perl) then
		foreach i (`jot - 7 5 -1`)
			if (-X perl$i) then
				# found the most current version of perl installed
				alias perl perl$i
				break
			endif
		end
		unset i
	endif

	# ncftp
	if (-X ncftp) then
		foreach i (`jot - 7 2 -1`)
			if (-X ncftp$i) then
				alias ncftp ncftp$i
				break
			endif
		end
		unset i
	endif
endif

# those that can't (be done by version number)
if (-X colorlynx) then
	alias lynx colorlynx
endif

if (-X lftp) then
	alias ftp lftp
	else if (-X tnftp) then
	alias ftp tnftp
endif

# other aliasing - some are just to save typing, others are to include default
# arguments and others to create pseudo commands. don't underestimate the
# power of single letter aliases when its almost 10 seconds RTT to the host.

# cd to the most recently modified directory in the current directory. 
# Why the slashes? The single quote prevents tcsh from expanding anything when
# the alias is created. When running the alias it is as if we just typed
# everything between the single quotes into the shell. That should mean that
# when the slashes reach the shell running the backquoted string they have
# been halved. That prevents that shell from processing the $ or " and so they
# are left for awk.
alias cdl 'cd `ls -lt | grep ^d | awk "NR == 1 { print \\$NF; found = 1 } END { if (! found) { print \\".\\" } }"`'

# http://www.ugh.net.au/~andrew/rm.html
#alias rcp '\cp' # (this one is too hard)
alias cp 'cp -i'
alias rmv '\mv'
alias mv 'mv -i'
alias rrm '\rm'
alias rm 'rm -i'

# ls
# we default to /bin/ls (as opposed to ls-F) for a number of reasons:
#	1) the speed difference (ls-F's claimed advantage) is minimal at least in
#		my basic tests
#	2) who needs F when you have colour (of course I'll change my mind next
#		time I have a black & white terminal)
#	3) /bin/ls handles non-printable characters better
#	4) /bin/ls can display setuid executables in different colours
# This may need revisiting on my next Solaris experience as I'm sure its' ls
# is <unpreffered>

# We do need to know if we are using GNU ls because some settings are
# different
ls --version |& grep -q GNU
if ( $? == 0 ) then
	# GNU ls
	alias ls ls -Nv --color=auto

	# this sets colours for GNU ls and ls-F
	setenv LS_COLORS 'di=34:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34'
else
	# probably BSD ls
	setenv CLICOLOR
	setenv LSCOLORS exfxcxdxbxeged
endif

# List all
alias la ls -a
# List all with unprintable characters displayed
alias lb la -b
# Long list (list with extra info)
alias ll ls -lA
# Long long list (list with even more info)
alias lll ll -o
# List with sizes
alias lss ls -s
# List directory attributes
alias lsd ll -d

if (-X alpine) then
	alias pine alpine
endif

# ps
set temp_backslash_quote = ${?backslash_quote}
set backslash_quote
set temp_awk_filter = '\'NR == 1 { print } ($0 ~ RE && !($3 == SHELL_PID && $0 ~ /SHELL_PID=SHELL_PID/)) { print } \''
if ($temp_backslash_quote) then
	set backslash_quote
endif
unset temp_backslash_quote
switch($OENV)
case 'solaris':
	set temp_ps_format = "user, pid, ppid, pcpu, pmem, rss, tty, s, stime, time, args"
	alias psg "ps -eo '$temp_ps_format' | egrep -e '^[ 	]*USE[R]|\!*' | $PAGER"
	alias psm "ps -eo '$temp_ps_format' | $PAGER"
	alias psu "ps -U $uid -o '$temp_ps_format'"
	breaksw
case 'linux':
	set temp_ps_format = "user pid ppid pcpu pmem rss tty s stime time cmd"
	alias psg "ps -e wwo '$temp_ps_format' | awk "${temp_awk_filter:q}" SHELL_PID=\$$ RE=\!:1 | $PAGER"
	alias psm "ps -e wwo '$temp_ps_format' | $PAGER"
	alias psu "ps xwwo '$temp_ps_format' | $PAGER"
	breaksw
case 'hpux':
	set temp_ps_format = "user,pid,ppid,pcpu,sz,tty,state,stime,time,args"
	alias psg "ps -eo '$temp_ps_format' | egrep -e '^USE[R]|\!*' | $PAGER"
	alias psm "ps -eo '$temp_ps_format' | $PAGER"
	alias psu "ps -U $uid -o '$temp_ps_format'"
	breaksw
case 'aix':
	set temp_ps_format = "user, pid, ppid, pcpu, pmem, tty, state, start, etime, args"
	alias psg "ps -eo '$temp_ps_format' | egrep -e '^[ 	]*USE[R]|\!*' | $PAGER"
	alias psm "ps -eo '$temp_ps_format' | $PAGER"
	alias psu "ps -U $uid -o '$temp_ps_format'"
	breaksw
case 'z/OS':
	set temp_ps_format = "user, pid, ppid, pcpu, tty, state, stime, etime,args"
	alias psg "ps -eo '$temp_ps_format' | egrep -e '^[      ]*USE[R]|\!*' | $PAGER"
	alias psm "ps -eo '$temp_ps_format' | $PAGER"
	alias psu "ps -U $uid -o '$temp_ps_format'"
	breaksw
default:
	set temp_ps_format = "user, pid, ppid, pcpu, pmem, rss, tt, state, start, time, command"
	alias psg "ps -axwwo '$temp_ps_format' | awk "${temp_awk_filter:q}" SHELL_PID=\$$ RE=\!:1 | $PAGER"
	alias psm "ps -axwwo '$temp_ps_format' | $PAGER"
	alias psu "ps -xwwo '$temp_ps_format' | $PAGER"
	breaksw
endsw
unset temp_ps_format temp_awk_filter

# Use regex when performing archie queries
alias archie "archie -r \!* | $PAGER"
# Get decimal support from bc and hide the copyright notice
switch($OENV)
case 'cygwin':
case 'freebsd':
case 'linux':
	alias bc bc -lq
	breaksw
default:
	alias bc bc -l
	breaksw
endsw
# This space intentionally left blank
if (-X bs) then
	alias bs 'bs -c -s'
endif
# ClearCase
if (-X cleartool) then
	alias ct cleartool
endif
# Report on user's diskusage
alias diskuse "repquota /home | tail -n +3 | grep -v root | sort -r +2 | $PAGER"
if (-X finger) then
	alias f finger
endif
# Flash terminal (to demonstrate unflash :)
alias flash 'printf \\\016'
alias g grep
if (-X gnome-open) then
	alias gopen gnome-open
endif

# Get compression stats from gzip and "create" a gunzip if no link exists
if (-X gzip) then
	alias gzip gzip -v
endif
if (-X gunzip) then
	alias gunzip gunzip -v
else
	if (-X gzip) then
		alias gunzip gzip -dv
	endif
endif
# Do the same for bzip2
if (-X bzip2) then
	alias bzip2 bzip2 -v
endif
if (-X bunzip2) then
	alias bunzip2 bunzip2 -v
else
	if (-X bzip2) then
		alias bunzip2 bzip2 -dv
	endif
endif

# Make h display just enough history to fit on a standard screen
if ($?prompt) then
	# we have a terminal so check it's size
	set temp_no_of_lines = "`echotc lines`" # the number of lines on the screen
	# make sure we have a number before doing calculations
	if ("XXX$temp_no_of_lines" ${temp_negative_glob} XXX*[^0-9]*) then
		@ temp_no_of_lines-- # make room for the prompt
	else
		# something is screwy - probably with our terminal - so just guess
		set temp_no_of_lines = 23
	endif
else
	# just give a reasonable figure
	set temp_no_of_lines = 23
endif
alias h history $temp_no_of_lines
unset temp_no_of_lines # clean up

# Move to the directory with the same path as your working directory had when
# you first moved into it (imagine someone removes the directory your are in
# and recreates it - you are now in an unlinked directory with no ..)
# or alternatively
# expand symlinks in the working directory path.
alias here 'pwd >& /dev/null && cd "`pwd`" || cd / && cd -'
alias j jobs -l
# killall makes me nervous so its good to know to some degree what happened
# at least you can probably know you stuffed up and go and fix it before
# anyone notices what went on
alias killall killall -v
# Quick aliases for ipv4 and ipv6
if ( -X netstat ) then
	switch ($OENV)
	case 'linux':
		alias n 'netstat -A inet'
		alias n6 'netstat -A inet6'
		breaksw
	case 'cygwin':
		# no real equivalent under windows AFAIK
		alias n 'netstat'
		alias n6 'netstat'
		breaksw
	default:
		alias n 'netstat -f inet'
		alias n6 'netstat -f inet6'
		breaksw
	endsw
else if ( -X ss ) then
	alias n 'ss -4'
	alias n6 'ss -6'
endif
# some machines have netcat by its full name
if (! -X nc) then
	if ( -X ncat ) then
		alias nc ncat
	else if ( -X netcat ) then
		alias nc netcat
	endif
endif
# Quick test and standardised command names (even though the Solaris way is
# probably more logical)
switch($OENV)
case 'hpux':
	alias p 'ping \!* -n 10'
	alias ping6 'ping -f inet6'
	alias p6 'ping6 \!* -n 10'
	breaksw
case 'solaris':
	alias p 'ping -s \!* 56 10'
	alias ping6 'ping -A inet6 -s'
	alias p6 'ping6 \!* 56 10'
	breaksw
case 'linux':
	alias ping6 'ping -6'
default:
	alias p 'ping -c 10'
	if (! -X ping6) then
		alias ping6 ping -6
	endif
	alias p6 'ping6 -c 10'
	breaksw
endsw
# Uninstalled perl docs
switch($OENV)
case 'darwin':
case 'freebsd':
	alias pman "pod2man \!^ | nroff -mandoc | $PAGER"
	breaksw
default:
	alias pman "pod2man \!^ | nroff -man | $PAGER"
    breaksw
endsw
# Recursive grep for those platforms without it
switch($OENV)
case 'aix':
case 'solaris':
	alias rgrep 'find . -exec grep -l \!^ {} \;'
	breaksw
default:
	alias rgrep 'grep -r'
	breaksw
endsw
# for machines without the command this does pretty much all I use (sets the
# title of the terminal window)
if (! -X settitle) then
	alias settitle 'set temp_title=""\!*""; if (! ${%temp_title}) set temp_title=${HOST:s,.,/,:h}; printf "]0;${temp_title}"; unset temp_title'
endif
# sqlplus is painful on its own
if (-X gqlplus) then
	alias sqlplus gqlplus
endif
# Keep my shell settings - they are the best so why use any others? Sadly
# doesn't work on Solaris or HPUX
alias rsu '\su'
switch($OENV)
case 'solaris':
case 'hpux':
	breaksw
default:
	alias su 'su -m'
	breaksw
endsw
# tar
if (-X bsdtar) then
	alias tar bsdtar
endif
# traceroute
if (-X traceroute) then
	alias t traceroute
else if (-X tracepath) then
		alias t tracepath
else if (-X tracert) then
		alias t tracert
endif
if (-X traceroute6) then
	alias t6 traceroute6
else if (-X tracert6) then
	alias t6 tracert6
endif
if (-X ytalk) then
	alias talk 'ytalk'
endif
# Length of time users have been logged in this month
alias ttyuse "ac -p | egrep -v 'total|ftp' | sort -rbn +1 | $PAGER"
# some aliases for making quick backups easier
alias ts_date	date '+%Y%m%d'
alias ts_time	date '+%H%M%S'
alias u uptime
# aka shiftin
alias unflash 'printf \\\017'
alias z suspend
