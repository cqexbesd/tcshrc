# .tcshrc file
#
# create a symbolic link from ~/.tcshrc to this file
# $Id: rc.tcsh,v 1.16 2009/10/08 19:07:17 andrew Exp $

# check version of tcsh before going any further
if ( (${tcsh:r:r} < 6) || ((${tcsh:r:r} == '6') && (${tcsh:r:e} < '07')) || ((${tcsh:r:r} == '6') && (${tcsh:r:e} == '07') && (${tcsh:e} < 4))) then
	echo "*** Please use tcsh version 6.07.04 or above (currently using $tcsh)" > /dev/stderr
	exit 0
endif

# save current environment for those system wide undocumented settings
if (! -f ~/.tcsh/previous_settings) then
	cat << HERE > ~/.tcsh/previous_settings
The World Before Shell Settings!

Path:
$path

Aliases:
`alias`

Shell Variables:
`set`

Environment:
`printenv`
HERE
endif

# there used to be a bug in tcsh where the meaning of =~ and !~ were seemingly
# reversed so determine which one we should use when.
if ( '123' =~ 12* ) then
	# all is well
	set temp_positive_glob = '=~'
	set temp_negative_glob = '!~'
else
	set temp_positive_glob = '!~'
	set temp_negative_glob = '=~'
endif

# determine our host environment so other things can act accordingly

# first we need to set OENV to something that allows us to differentiate
# between OSs and environments. We used to test OSTYPE but Cygwin sets this to
# posix and it might not be the only one so its not safe to assume posix ==
# cygwin.
if (${?OSTYPE}) then
	if (${?OS}) then # no short circuiting in tcsh
		if (${OSTYPE} == "posix" && "${OS}" == 'Windows_NT') then
			setenv OENV 'cygwin'
		else
			setenv OENV ${OSTYPE}
		endif
	else if (${?HOSTTYPE}) then
		if (${OSTYPE} == "posix" && "${HOSTTYPE}" == 'z/OS') then
			setenv OENV 'z/OS'
		else
			setenv OENV ${OSTYPE}
		endif
	else
		setenv OENV ${OSTYPE}
	endif
else
	# OSTYPE is unset - very rarely seen luckily. try for uname in the default
	# path and hope for the best
	if (-X uname) then
		setenv OENV `uname -s`
	else
		echo "*** Unable to determine operating environment" > /dev/stderr
		exit 0
	endif
endif

# now we set OENVVER to indicate the version of OENV
if (-X uname) then
	switch ($OENV)
	case 'aix':
		setenv OENVVER "`uname -v`.`uname -r`"
		breaksw
	case 'hpux':
		set temp_oenvver = `uname -r`
		setenv OENVVER ${temp_oenvver:s#.#/#:t}
		unset temp_oenvver
		breaksw
	case 'FreeBSD':
		set temp_oenvver = `uname -r`
		setenv OENVVER ${temp_oenvver:s/-/./:r}
		unset temp_oenvver
		breaksw
	default:
		setenv OENVVER `uname -r`
		breaksw
	endsw
endif

# now we make sure ARCH is set
if (! ${?ARCH} ) then
	switch ($OENV)
		case 'aix':
			# uname -p works for AIX5
			if (${OENVVER:r} >= 5) then
				setenv ARCH `uname -p`
			endif
			# lsattr -El proc0 | grep PowerPC looks ok for AIX4 but to much
			# effort for now as AIX4 isn't very common anymore
			breaksw
		case 'hpux':
			# well something like this...
			if (-X uname) then
				if ("`uname -m`" == "ia64") then
					setenv ARCH hpia
				else
					setenv ARCH hppa
				endif
			endif
			breaksw
		case 'linux':
			if (-X arch) then
				setenv ARCH `arch`
			else
				if (-X uname) then
					# Arch Linux gives "unknown" for uname -p...
					set temp_poss_arch="`uname -p`"
					if ("${temp_poss_arch}" == "unknown") then
						set temp_poss_arch="`uname -m`"
					endif
					setenv ARCH "${temp_poss_arch}"
					unset temp_poss_arch
				endif
			endif
			breaksw
		case 'z/OS':
			setenv ARCH `uname -s`
			breaksw
		default:
			setenv ARCH `uname -p`
			breaksw
	endsw
endif

# host triplet (and duplet) variables
setenv HOST_DOUBLET "${OENV}-${OENVVER}"
if (${?ARCH}) then
	setenv HOST_TRIPLET "${OENV}-${OENVVER}-${ARCH}"
endif

# determine our hostname
if (! ${?HOST}) then
	if (-X uname) then
		set tmp_name = `uname -m`
	else if (-X hostname) then
		set tmp_name = `hostname -s`
	else if (-X sysctl) then
		switch ($OENV)
			case 'darwin':
			case 'freebsd':
				set tmp_name = `sysctl -n kern.hostname`
				breaksw
			case 'linux'
				set tmp_name = `sysctl -n kernel.hostname`
				breaksw
		endsw
	endif
	if (${?tmp_name}) then
		setenv HOST "${tmp_name:ar}"
	else
		setenv HOST ""
	endif
	unset tmp_name
endif

# pull in our config file to allow common config changes without hacking
if (-f ~/.tcshrc.conf) then
	source ~/.tcshrc.conf
endif

# set up the world for non-interactive shells
if (! $?prompt) then
	# pull in our common stuff
	source ~/.tcsh/commands_and_environment.tcsh

	# set the umask to something quite conservative as we aren't here
	# to keep an eye on things
	umask 0077

	# clear up our name space
	unset 'prefs_*'

	# that's it for us!
	exit 0
endif

# set up the world for interactive shells
source ~/.tcsh/commands_and_environment.tcsh
source ~/.tcsh/features.tcsh
source ~/.tcsh/complete.tcsh
source ~/.tcsh/termio.tcsh

if ($?loginsh) then
	# run our once only code
	source ~/.tcsh/login.tcsh
endif

# allow for local hacks
if (-f ~/.tcshrc.local) then
	source ~/.tcshrc.local
endif

# clear up our name space
unset temp_positive_glob
unset temp_negative_glob
unset prefs_*
unsetenv CSHRC_BEEN_RUN
