# tab completions
#
# $Id: complete.tcsh,v 1.37 2013/07/19 17:12:58 andrew Exp $

complete {alias,unalias} 'p/1/a/'
complete apachectl 'p/1/(start stop restart graceful configtest)/'
complete bindkey 'p/2/b/'
complete bunzip2 'p/*/f:*.{bz2,tbz}/'
complete {cd,pushd} 'p/1/d/'

# CVS
if ($?prefs_cvs_repos) then
	set temp_cvs_repos = "(${prefs_cvs_repos})"
else
	set temp_cvs_repos = 'x::method:host:path (or just path for local)'
endif
complete cvs 'C/-/(-d -R -z -D)/' 'n/-z/`jot 9`/' "n|-d|$temp_cvs_repos|" 'n/update/(-dP)/' 'p/*/`echo "update commit diff export log add remove status"; \ls`/'
unset temp_cvs_repos

# ClearTool/ClearCase
if (-X cleartool) then
	foreach temp_alias (cleartool ct)
		complete ${temp_alias} 'p/1/(catcs edcs setcs ls lsco mkelem mkview rmview ci co uncheckout describe diff setview startview lsview lsvtree lsprivate lshistory)/' 'n/diff/(-pred -g)/' 'n/setview/`cleartool lsview -short`/' 'n/startview/`cleartool lsview -short`/'
	end
endif
unset temp_alias

# defaults
switch($OENV)
case 'darwin':
	# alas I get "Word too long." if I use this
	#alias list_all_domains 'defaults domains | awk -F", " \'{ for (i = 1; i <= NF; ++i) { printf("\\\\\"%s\\\\\"\n", $i) } }\''
	#set all_domains = "`list_all_domains`"
	complete defaults						\
		'c/-/(currentHost host)/'			\
		"n/read/`defaults domains`/"		\
		"n/read-type/`defaults domains`/"	\
		"n/write/`defaults domains`/"		\
		"n/rename/`defaults domains`/"		\
		"n/delete/`defaults domains`/"		\
		'p/1/(read read-type write rename delete domains find help)/'
	breaksw
endsw

complete setenv 'p/1/e/'
complete unsetenv 'n/*/e/'
complete printenv 'p/1/e/'
complete gcc 'C/-/(-Wall -lm -lutil -o -lstdc++)/' 'p/*/f:*.[cm]*/'
complete gpg 'C/-/(--armor --decrypt --decrypt-files --delete-key --edit-key --encrypt --encrypt-files --export --fingerprint --import --list-keys --symmetric --recv-keys --verify)/' 'p/*/f/'
complete gunzip 'p/*/f:*.{gz,tgz}/'
complete kill 'c/-/S/'
complete limit 'p/1/l/' 'p/2/(unlimited)/'
complete man 'p/*/c/'
complete make 'c/-/(DDEBUG DWITHOUT_X11)/' 'n/*/(all clean depend extract fetch fetch-list install package patch)/'
complete ndc 'n/*/(start stop restart reload)/'
switch($OENV)
case 'darwin':
	complete open 'c/-/(a e)/'
	breaksw
endsw
complete postfix 'n/*/(start stop reload abort flush check)/'

# ppp
switch ($OENV)
case 'FreeBSD':
	alias list_ppp_profiles 'grep -E \'^\w+:$\' /etc/ppp/ppp.conf | grep -v \'^default:$\' | sed \'s/:$//\''
	complete ppp 'c/-/(background direct nat)/' 'p/*/`list_ppp_profiles`/'
	breaksw
endsw

complete set 'c/*=/f/' 'p/1/s/=' 'n/=/f/'
complete unset 'p/*/s/'
switch ($OENV)
case 'FreeBSD':
	complete sysctl 'n/*/`sysctl -Na`/'
	breaksw
endsw
complete tar 'p/1/(-xzvf -czvf -tzvf -xvf -cvf -tf -xyvf -cyvf -tyf)/' 'p/2-/f/'
complete gtar 'p/1/(-xzvf -czvf -tzvf -xvf -cvf -tf -xyvf -cyvf -tyf)/' 'p/2-/f/'
complete uncompress 'p/*/f:*.Z/'
complete which 'p/1/c/'

### Network Stuff
if ($?prefs_hostnames) then
	complete p "n/*/($prefs_hostnames)/"
	complete t "n/*/($prefs_hostnames)/"
	complete t6 "n/*/($prefs_hostnames)/"
	complete telnet "n/*/($prefs_hostnames)/"
	complete ftp "n/*/($prefs_hostnames)/"
	complete xhost "c/+/($prefs_hostnames)/" "c/-/($prefs_hostnames)/" 'n/*/(+)//'
	# ssh/scp
	#
	# tcsh doesn't support completing from the union of a list of words and
	# the regular files completion so this horror had to be born.
	#
	# Do get scp to complete files when it doesn't want to try starting with
	# "./"
	set temp_user_at = ""
	if ($?prefs_usernames) then
		foreach temp_user ($prefs_usernames)
			set temp_user_at = ($temp_user_at "${temp_user}@")
		end
	endif
	complete ssh "C/-/(-4 -6 -i -p -v -N -L -R)/" "n/-L/x:<lport:rhost:rport>/" "n/-R/x:<rport:lhost:lport>/" "n/-i/f/" "c/*@/($prefs_hostnames)//" "p/*/($temp_user_at $prefs_hostnames)//"

	set temp_host_colon = ""
	foreach temp_host ($prefs_hostnames)
		set temp_host_colon = ($temp_host_colon ${temp_host}:)
	end

	complete scp "C/-/(-4 -6 -i -v -C -P)/" "n/-i/f/" "C@*/*@f@" "c/*:/f/" "c/*@/($temp_host_colon)//" "n/*:*/f/" "p/*/($temp_user_at $temp_host_colon)//"

	unset temp_user_at temp_user temp_host_colon temp_host
endif
