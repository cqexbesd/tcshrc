# Set up terminal characteristics and key bindings
#
# $Id: termio.tcsh,v 1.15 2012/11/27 15:55:27 andrew Exp $

# use emacs by default (bizzare for a vi user I know)
bindkey -e
# make ^w just delete the last word
bindkey -b ^w backward-delete-word
# ^k to do the same forward
bindkey -b ^k delete-word
# cycle through possible completions with ESC-TAB
bindkey "\e\t" complete-word-fwd
# and go back with ESC-`
bindkey "^[`" complete-word-back
# free up ^y for delayed suspend (I never used tcsh cut and paste anyway)
bindkey -r ^y
# use ^Z to bring our editor back to the foreground
bindkey ^z run-fg-editor
# expand history substitutions as we go
bindkey " " magic-space
switch($OENV)
case 'FreeBSD':
	stty kerninfo
	stty erase2 
	breaksw
case 'darwin':
	stty kerninfo
	if (${OENVVER:r:r} == 8) then
		# this feature didn't last long
		stty erase2 
	endif
	breaksw
endsw
# restart halted output with any key
stty ixany
# make sure these settings are sane (looking at you HP)
stty kill 
stty erase 
stty intr 
